<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class User extends Eloquent
{
    protected $connection = 'mongodb';
	protected $collection = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone', 'site', 'password',
    ];

    public function users()
    {
        return $this->hasMany(Payment::class);
    }

    public function scopeSearch($query)
    {
        return;
    }
}
