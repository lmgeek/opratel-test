<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required',
            'site' => 'required',
            'password' => 'required',
        ];
    }


    protected $messages = [
        'phone.required' => 'The field phone is required.',
        'phone.numeric' => 'The field phone only acepted numbers.',
        'site.required' => 'The field site is required.',
        'password.required' => 'The field password is required.',

    ];
}
