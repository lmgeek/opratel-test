<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class Payment extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'payment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone', 'amount',
    ];


    public function payments()
    {
        return $this->belongsTo(User::class);
    }
}
